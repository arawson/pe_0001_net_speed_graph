#!/bin/bash
#Metrics
#Side notes:
#	It seems like the best way to develop this would be in cool retro term.
#		That's also the best way to show it off in videos.
#	See how CouchDB performs on raspberry pi 2.

# Tunnelling:
# Need to be able to get access to couchdb from remote hosts.
# To do this securely, SSH tunnelling will be used.
# If a remote host is compromised, its ssh key can be revoked.
# In addition, its user can have minimal privileges.
#this was used in dev
ssh -l etl -N -L 5984:localhost:5984 dev-iot-01

#
#Shell questions:
#	Is it possible for the shell to keep a handle to background a program and then terminate the program when the script is done?
#	https://www.digitalocean.com/community/tutorials/how-to-use-bash-s-job-control-to-manage-foreground-and-background-processes
#		open an ssh tunnel for the db port 'ssh blah blah &'
#		jobs shows the running jobs, maybe query running jobs in order to find ssh tunnel command?
#		can use 'kill %somenumber' to kill a job
#		shouldn't use the normal job control commands, they are meant for interactive mode
#		instead the script can use pids directly
#		how do i get the pid of the tunnel command?
#			http://unix.stackexchange.com/questions/46235/how-does-reverse-ssh-tunneling-work
#			can still run command with &
#			pid can be captured with $! - guess thats last pid
#			yep
#			ssh blah blah &
#			TUNNEL_PID=$!
#			#do some stuff
#			kill $TUNNEL_PID
#	parsing arguments
#	http://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
#
#Ideas for data tracking some stuff on couchDB:
#Network
#	Ping
#	Down/Up Speed
#		special: if the db is hosted externally, then there needs to be a script monitoring the db
#			and checking for results, which adds a "network down" document for that time period
#			or database replication up to the cloud
#	Warnings
#		special: add warning documents on errors in the log
#			add warning documents when other etls don't ping for too long
#Servers
#	Generic Servers
#		CPU, RAM, uptime, disk IO, net IO, disk free space, running processes, active users
#	Generic Server Processes
#		CPU usage, RAM usage, disk IO usage, net IO usage, running, errors in log
#		log:
#			journalctl, flat file
#	Game Servers
#		Minecraft
#			World size, connected players, frequency of chat messages, server error messages, server version,
#			new server version available?
#		Starbound
#			World size, ????
#		Terraria
#			World size,
#		VPN for lan
#			connected users
#	Blog Server
#		Days since last post
#	Owncloud Server
#		recently updated files, free space remaining, dirstat, number of notes taken
#		Outbound integration so that information from the monitoring server shows up in owncloud
#			libreoffice or html graphs if possible
#			is there some api that programs can use to upload to owncloud?
#		
#Sensors
#	temperature, light
#Cloud
#	Source Code
#		active repos
#		specific repos
#			git version, tags, unit test results
#
#Implementation Details:
#ZeroMQ or RabbitMQ might be useful to shuttle messages from the sensor system to the bash scripts.
#
#Ideas for output (how am i going to use this data?):
#
#
#
#
#Updated structure for repo:
#/
#/config
#/config/metrix/ping.json
#/run_config/evochamber01.json
#/run_config/lurker.json
#/ping.sh
#/init.sh
#/



#!/bin/bash
# Use -gt 1 to consume two arguments per pass in the loop (e.g. each
# argument has a corresponding value to go with it).
# Use -gt 0 to consume one or more arguments per pass in the loop (e.g.
# some arguments don't have a corresponding value to go with it such
# as in the --default example).
# note: if this is set to -gt 0 the /etc/hosts part is not recognized ( may be a bug )
while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -e|--extension)
    EXTENSION="$2"
    shift # past argument
    ;;
    -s|--searchpath)
    SEARCHPATH="$2"
    shift # past argument
    ;;
    -l|--lib)
    LIBPATH="$2"
    shift # past argument
    ;;
    --default)
    DEFAULT=YES
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done
echo FILE EXTENSION  = "${EXTENSION}"
echo SEARCH PATH     = "${SEARCHPATH}"
echo LIBRARY PATH    = "${LIBPATH}"
echo "Number files in SEARCH PATH with EXTENSION:" $(ls -1 "${SEARCHPATH}"/*."${EXTENSION}" | wc -l)
if [[ -n $1 ]]; then
    echo "Last line of file specified as non-opt/last argument:"
    tail -1 $1
fi
