#!/bin/bash

## Usage: log_wrapper TAG CMD ARGS...

TAG="$1"

shift

CMD="$1"

shift

$CMD $@ 2>&1 | logger -t $TAG

