#!/bin/bash
#
# init.sh
# Copyright (C) 2016 Aaron Rawson <aaronmrawson@gmail.com>
#
# Distributed under terms of the GPLv3 license.
#

CONFIG_FILE=UNDEFINED
DESIGN_DIR=UNDEFINED

while [[ $# -gt 1 ]]; do
    key="$1"

    case $key in
        --cc) #connection config
        CONFIG_FILE=$2
        shift # past argument
        ;;
        --dd)
        DESIGN_DIR=$2
        shift # past argument
        ;;
        *)
        echo Unknown option: $key
        ;;
    esac
    shift # past argument or value
done

if [ "$(uname)" == "Darwin" ]; then
    #mac os x unsupported, don't got no machine to test on
    echo mac os is unsupported
    exit 1
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    #we are on gnu linux
    echo on linux
elif [ "$(expr substr $(uname -s) 1 5)" == "MINGW" ]; then
    #on windows mingw, crazy!
    echo on mingw
fi

################################################################################
# SCRIPT CONFIGURATION VARIABLES
if [ "$CONFIG_FILE" == "UNDEFINED" ]; then
    echo Error: No connection config file specified
    exit 1
fi

if [ ! -d "$DESIGN_DIR" ] || [ "$DESING_DIR" == "UNDEFINED" ]; then
    echo Error: A valid directory must be set to output the files to
    exit 1
fi

################################################################################
# LOAD CONFIG PARAMETERS
DB_HOSTNAME="$(jq -r '.hostname' $CONFIG_FILE)"
DB_PORT="$(jq -r '.port' $CONFIG_FILE)"
DB_PROTOCOL="$(jq -r '.protocol // "http"' $CONFIG_FILE)"
DB_NAME="$(jq -r '.dbname' $CONFIG_FILE)" #the database on the dbms
DB_CONNECT_STR="$DB_PROTOCOL://$DB_HOSTNAME:$DB_PORT/"
METRIX_CONNECT_STR="$DB_CONNECT_STR/$DB_NAME"

################################################################################
# ATTEMPT DATABASE CONNECTION
echo Attempting to connect to $DB_CONNECT_STR ...
DB_HELLO=$(curl -s $DB_CONNECT_STR)
DB_VERSION="$(echo $DB_HELLO | jq -r '.version // "false"')"

if [ $DB_VERSION == "false" ]; then
    echo Failed to connect to couchdb ...
    exit 1
else
    echo Successfully connected to couch db version $DB_VERSION ...
fi


################################################################################
# MAKE SURE DATABASE EXISTS
METRIX_STATUS=$(curl -s $DB_CONNECT_STR/$DB_NAME)
METRIX_STATUS_ERROR="$(echo $METRIX_STATUS | jq -r '.error // false')"

if [ $METRIX_STATUS_ERROR != "false" ]; then
    echo Metrix database does not exist, creating it ...
    METRIX_PUT_RESULT=$(curl -s -X PUT $DB_CONNECT_STR/$DB_NAME)
    METRIX_PUT_OK=$(echo $METRIX_PUT_RESULT | jq -r '.ok // "false"')
    if [ $METRIX_PUT_OK != "false" ]; then
        echo Metrix database created ok, chcking ping config ...
    else
        echo Unable to create metrix database ...
        echo Error:
        echo $METRIX_PUT_RESULT
        exit 1
    fi
else
    echo Metrix database exists, checking ping config ...
fi

################################################################################
# GET DOCS TO EXTRACT
DOCS_JSON=$(curl -s --globoff -X GET "$METRIX_CONNECT_STR/_all_docs?startkey=%22_design/%22&endkey=%22_design0%22&include_docs=true")
DOCS_COUNT=$(echo $DOCS_JSON | jq '.rows | length')

echo Have $DOCS_COUNT design doc\(s\) to extract ...

index=0
DOC=""

advance_doc() {
    JQL=".rows[$index]"
    DOC=$(echo "$DOCS_JSON" | jq -r $JQL)
    ((index++))
}

advance_doc

while [ "$DOC" != "null" ]; do
    FILENAME="$(echo $DOC | jq -r '.id | gsub(".*/"; "")')".json
    echo $DOC | jq '.doc' | sed '/"_rev".*/d' > $DESIGN_DIR/$FILENAME
    advance_doc
done

























