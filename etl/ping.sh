#!/bin/bash
#
# ping.sh
# loads ping data to the couchdb server configured in the file and then
# optionally outputs graphs
# Copyright (C) 2016 Aaron Rawson <aaronmrawson@gmail.com>
#
# Distributed under terms of the GPLv3 license.
#

CONFIG_FILE=UNDEFINED
SYSTEM_CONFIG_FILE=UNDEFINED

#usage etl/ping.sh -c config/dev.json
while [[ $# -gt 1 ]]; do
    key="$1"

    case $key in
        --cc) # connection config
        CONFIG_FILE=$2
        shift # past argument
        ;;
        --sc) # system config
        SYSTEM_CONFIG_FILE=$2
        shift # past argument
        ;;
        *)
        echo Unknown option: $key
        ;;
    esac
    shift # past argument or value
done

SYSTEM_NAME=UNDEFINED

if [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    #we are on gnu linux
    SYSTEM_NAME=linux
elif [ "$(expr substr $(uname -s) 1 5)" == "MINGW" ]; then
    #on windows mingw, crazy!
    SYSTEM_NAME=mingw
    PING_CMD="ping -n"
fi

if [ "$SYSTEM_NAME" == "UNDEFINED" ]; then
    echo Error: unsupported system: $SYSTEM_NAME
    exit 1
fi

################################################################################
# SCRIPT CONFIGURATION VARIABLES
if [ "$CONFIG_FILE" == "UNDEFINED" ]; then
    echo Error: No connection config file specified
    exit 1
fi

if [ "$SYSTEM_CONFIG_FILE" == "UNDEFINED" ]; then
    echo Error: No system config file specified
    exit 1
fi

################################################################################
# LOAD SYSTEM CONFIG VARIABLES

if [ "$(jq -r '.system_name' $SYSTEM_CONFIG_FILE)" != "$SYSTEM_NAME" ]; then
    echo System name: $SYSTEM_NAME does not match value from system config file...
    exit 1
fi

PING_CMD="$(jq -r '.ping_cmd' $SYSTEM_CONFIG_FILE)"
PING_SED_CMD="$(jq -r '.ping_sed_cmd' $SYSTEM_CONFIG_FILE)"
#PING_SED_CMD="/.*mdev = /{;s///;s/ms.*//;s/^/min=/;s/\// avg=/;s/\// max=/;s/\/.*//;}"
DATE_ARGS="$(jq -r '.date_args' $SYSTEM_CONFIG_FILE)"
PING_DATE_TIME="$(date $DATE_ARGS | jo -a)"

################################################################################
# VERIFY NECESSARY FILES
#check existence of config file
if [ ! -e "$CONFIG_FILE" ]; then
    echo Config file \($CONFIG_FILE\) is missing! Must abort...
    exit 1
fi

################################################################################
# LOAD CONFIG PARAMETERS
DB_HOSTNAME="$(jq -r '.hostname' $CONFIG_FILE)"
DB_PORT="$(jq -r '.port' $CONFIG_FILE)"
DB_PROTOCOL="$(jq -r '.protocol // "http"' $CONFIG_FILE)"
DB_NAME="$(jq -r '.dbname' $CONFIG_FILE)" #the database on the dbms
DB_CONNECT_STR="$DB_PROTOCOL://$DB_HOSTNAME:$DB_PORT/"
METRIX_CONNECT_STR="$DB_CONNECT_STR/$DB_NAME"

################################################################################
# ATTEMPT DATABASE CONNECTION
echo Attempting to connect to $DB_CONNECT_STR ...
DB_HELLO=$(curl -s $DB_CONNECT_STR)
DB_VERSION="$(echo $DB_HELLO | jq -r '.version')"

echo Successfully connected to couch db version $DB_VERSION ...

################################################################################
# MAKE SURE DATABASE EXISTS
METRIX_STATUS=$(curl -s $DB_CONNECT_STR/$DB_NAME)
METRIX_STATUS_ERROR="$(echo $METRIX_STATUS | jq -r '.error // false')"

if [ $METRIX_STATUS_ERROR != "false" ]; then
    echo Error gettign metrix database: $METRIX_STATUS_ERROR ...
    echo Does the $DB_NAME exist?
    exit 1
else
    echo Metrix database exists, proceding ...
fi

################################################################################
# GET PING CONFIG
PING_CONFIG=$(curl -s $METRIX_CONNECT_STR/ping.json)
#bug? feature? looks like piping to sed here cleans up the first row of results
PING_HOSTS=( $(echo $PING_CONFIG | jq -r '.hosts[]' | sed '') )
PING_COUNT="$(echo $PING_CONFIG | jq -r '.ping_count // 10')"
PING_HOST_COUNT="${#PING_HOSTS[@]}"
PING_UUID="$(curl -s -X GET $DB_CONNECT_STR/_uuids | jq -r '.uuids[]')"
PING_RESULTS=()

################################################################################
# PING THE HOSTS
echo Will ping $PING_HOST_COUNT hosts ...
index=0
while [ "$index" -lt "$PING_HOST_COUNT" ]; do
#for host in ${PING_HOSTS[@]}; do
    host=${PING_HOSTS[$index]}
    echo Attempting to reach $host ...
    $PING_CMD $PING_COUNT $host | tail -1 | sed "$PING_SED_CMD"
    $PING_CMD $PING_COUNT $host | tail -1 | sed "$PING_SED_CMD" | jo
    #the cool part about piping it is that if the command fails jo just outputs {}
    PING_RESULTS[$index]=$(echo | jo $($PING_CMD $PING_COUNT $host | tail -1 | sed "$PING_SED_CMD"))
    #echo ${PING_RESULTS[$index]}
    ((index++))
done

PING_JSON=""
index=0
while [ "$index" -lt "$PING_HOST_COUNT" ]; do
    PING_JSON="$PING_JSON ${PING_HOSTS[$index]}=${PING_RESULTS[$index]}"
    ((index++))
done

PING_DOCUMENT="$(jo test=ping datetime=$PING_DATE_TIME $PING_JSON)"

echo Uploading test results to db ...

PING_UPLOAD_RESULT="$(curl -s -X PUT $METRIX_CONNECT_STR/$PING_UUID -d $PING_DOCUMENT)"
PING_UPLOAD_OK="$(echo $PING_UPLOAD_RESULT | jq -r '.ok // false')"
if [ $PING_UPLOAD_OK == "false" ]; then
    echo Failed to upload to db ...
    echo Error:
    echo $PING_UPLOAD_RESULT
    exit 1
fi
echo Upload successful ...
echo Process done ...

exit 0

