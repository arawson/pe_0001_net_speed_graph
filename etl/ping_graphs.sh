#! /bin/sh
#
# ping_graphs.sh
# Copyright (C) 2017 Aaron Rawson <aaronmrawson@gmail.com>
#
# Distributed under terms of the GPLv3 license.
#

#this will query the design doc for pings to lurker over all time
#curl -X GET "http://localhost:5984/metrix/_design/ping/_view/by_host_datetime?startkey=\[\"lurker\",0\]&endkey=\[\"lurker\",9999\]"

#still need:
# config document describing which hosts to make graphs for
# pull out common code
# thing to generate graphs
# graph web page template

