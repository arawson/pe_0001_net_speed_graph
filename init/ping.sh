#!/bin/bash
#
# init.sh
# Copyright (C) 2016 Aaron Rawson <aaronmrawson@gmail.com>
#
# Distributed under terms of the GPLv3 license.
#

CONFIG_FILE=UNDEFINED
PING_CONFIG_FILE=UNDEFINED

#usage etl/ping.sh -c config/dev.json
while [[ $# -gt 1 ]]; do
    key="$1"

    case $key in
        --cc) #connection config
        CONFIG_FILE=$2
        shift # past argument
        ;;
        --ic) #init config
        PING_CONFIG_FILE=$2
        shift # past argument
        ;;
        *)
        echo Unknown option: $key
        ;;
    esac
    shift # past argument or value
done

if [ "$(uname)" == "Darwin" ]; then
    #mac os x unsupported, don't got no machine to test on
    echo mac os is unsupported
    exit 1
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    #we are on gnu linux
    echo on linux
elif [ "$(expr substr $(uname -s) 1 5)" == "MINGW" ]; then
    #on windows mingw, crazy!
    echo on mingw
fi

################################################################################
# SCRIPT CONFIGURATION VARIABLES
if [ "$CONFIG_FILE" == "UNDEFINED" ]; then
    echo Error: No connection config file specified
    exit 1
fi

if [ "$PING_CONFIG_FILE" == "UNDEFINED" ]; then
    echo Error: No init ping config file specified
    exit 1
fi


################################################################################
# VERIFY NECESSARY FILES
#check existence of config file
if [ ! -e "$CONFIG_FILE" ]; then
    echo Config file \($CONFIG_FILE\) is missing! Must abort...
    exit 1
fi

################################################################################
# LOAD CONFIG PARAMETERS
DB_HOSTNAME="$(jq -r '.hostname' $CONFIG_FILE)"
DB_PORT="$(jq -r '.port' $CONFIG_FILE)"
DB_PROTOCOL="$(jq -r '.protocol // "http"' $CONFIG_FILE)"
DB_NAME="$(jq -r '.dbname' $CONFIG_FILE)" #the database on the dbms
DB_CONNECT_STR="$DB_PROTOCOL://$DB_HOSTNAME:$DB_PORT/"
METRIX_CONNECT_STR="$DB_CONNECT_STR/$DB_NAME"

################################################################################
# ATTEMPT DATABASE CONNECTION
echo Attempting to connect to $DB_CONNECT_STR ...
DB_HELLO=$(curl -s $DB_CONNECT_STR)
DB_VERSION="$(echo $DB_HELLO | jq -r '.version // "false"')"

if [ $DB_VERSION == "false" ]; then
    echo Failed to connect to couchdb ...
    exit 1
else
    echo Successfully connected to couch db version $DB_VERSION ...
fi


################################################################################
# MAKE SURE DATABASE EXISTS
METRIX_STATUS=$(curl -s $DB_CONNECT_STR/$DB_NAME)
METRIX_STATUS_ERROR="$(echo $METRIX_STATUS | jq -r '.error // false')"

if [ $METRIX_STATUS_ERROR != "false" ]; then
    echo Metrix database does not exist, creating it ...
    METRIX_PUT_RESULT=$(curl -s -X PUT $DB_CONNECT_STR/$DB_NAME)
    METRIX_PUT_OK=$(echo $METRIX_PUT_RESULT | jq -r '.ok // "false"')
    if [ $METRIX_PUT_OK != "false" ]; then
        echo Metrix database created ok, chcking ping config ...
    else
        echo Unable to create metrix database ...
        echo Error:
        echo $METRIX_PUT_RESULT
        exit 1
    fi
else
    echo Metrix database exists, checking ping config ...
fi

################################################################################
# CREATE PING CONFIG IF IT IS NOT THERE
PING_CFG_ID="ping.json"
PING_CFG_STATUS=$(curl -s -X GET $METRIX_CONNECT_STR/$PING_CFG_ID)
PING_CFG_ERROR=$(echo $PING_CFG_STATUS | jq -r '.error // "false"')

if [ $PING_CFG_ERROR == "false" ]; then
    echo Ping config already exists ...
else
    echo Creating ping config ...
    PING_CFG_DOC="$(jq -c '.' $PING_CONFIG_FILE)"
    echo $PING_CFG_DOC
    PING_CFG_CREATE_STATUS=$(curl -s -X PUT $METRIX_CONNECT_STR/$PING_CFG_ID -d $PING_CFG_DOC)
    PING_CFG_CREATE_OK=$(echo $PING_CFG_CREATE_STATUS | jq -r '.ok // "false"')
    if [ $PING_CFG_CREATE_OK == "false" ]; then
        echo Failed to create ping config ...
        echo Error:
        echo $PING_CFG_CREATE_STATUS
        exit 1
    else
        echo Created ping config successfully
    fi
fi

echo Done ...
exit 0

